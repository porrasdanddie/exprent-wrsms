function solveInputs(due, given, latest) {
  let inputs = [due, given, latest]
  for (let input of inputs) {
    if(input === "") {
      input = 0
    }
  }
  return (parseInt(inputs[0]) + parseInt(inputs[2])) - inputs[1]
}

module.exports = solveInputs