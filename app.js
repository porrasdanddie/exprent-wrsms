const express = require('express')
const server = express()
const path = require('path')
const ObjectId = require("mongodb").ObjectId
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
// const bcrypt = require('bcrypt')
const client = require('mongodb').MongoClient
const url = process.env.MONGODB_URI || "mongodb://localhost:27017/se3103project"
const port = process.env.PORT || 8080
const handleCustomerTasks = require('./routes/customerTasks.js')
const handleEmployeeTasks = require('./routes/employeeTasks.js')
const handlePayableTasks = require('./routes/payableTasks.js')
const handleTransactionTasks = require('./routes/transactionTasks.js')
server
  .use(express.static(path.join(process.cwd(), "public")))
  .use(bodyParser.urlencoded({ extended: true }))
  .use(bodyParser.json())
  .listen(port, () => {
  console.log(`App is at http://localhost:${port}`)
  })

client.connect(url, (connectionError, db) => {
  if (connectionError) throw connectionError
  handleCustomerTasks(server, ObjectId, db)
  handleEmployeeTasks(server, ObjectId, db)
  handlePayableTasks(server, ObjectId, db)
  handleTransactionTasks(server, ObjectId, db)
  server
    .get("/showDashboard", (request, response) => {
      db.collection("customers").find().toArray((error, customers) => {
        if (error) throw error
        db.collection("employees").find().toArray((error, employees) => {
          if (error) throw error
          response.json([customers, employees])
          response.end()
        })
      })
    })
    
    .get("/retrieveDocuments/:collection", (request, response) => {
      const collection = request.params.collection
      db.collection(collection).find().toArray((error, documents) => {
        if (error) throw error
        response.json(documents)
      })
    })

    .get("/deleteDocument/:objectId&:collection", (request, response) => {
      const objectId = request.params.objectId
      const id = new ObjectId(objectId)
      const collection = request.params.collection
      db.collection(collection).deleteOne({ _id: id }, (error, result) => {
        db.collection(collection).find().toArray((error, documents) => {
          response.json(documents)
        })
      })
    })

    .get("/retrieveDocument/:objectId&:collection", (request, response) => {
      const objectId = request.params.objectId
      const id = new ObjectId(objectId)
      const collection = request.params.collection
      db.collection(collection).findOne({ _id: id }, (error, document) => {
        if (error) throw error
        response.json(document)
      })
    })

    .get("/retrieveDocumentUsingName/:name&:collection", (request, response) => {
      const name = request.params.name
      const collection = request.params.collection
      db.collection(collection).findOne({ name: name }, (error, document) => {
        response.json(document)
      })
    })

    .post("/loginAdmin", (request, response) => {
      const userID = request.body.user
      const password = request.body.pass
      db.collection("admins").findOne({userID: userID, password: password}, (error, result) => {
        if (error) throw error 
        if (result != null) {
          jwt.sign({ userID, password }, "secret", (error, token) => {
            response.json({ jwt: token })
          })
        }
        else {
          response.json({ success: false })
        }
      })
    })
 
    .get("/private", (request, response) => {
      const authHeader = request.header("Authorization")
      jwt.verify(extract(authHeader), "secret", (error, decoded) => {
        if (error) {
          response.statusCode = 403
          response.json({ valid: false })
        }
        else {
          response.json({ header: authHeader, currentUser: decoded.user})
        }
      })
    })
})
