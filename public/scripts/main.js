window.onload = () => {
  const container = document.getElementById("content-container")
  container.innerHTML = document.getElementById("log-in-screen").innerHTML
  document.getElementById("header").style.display = "none"

  if (localStorage.getItem("jwtToken") != null) {
    container.innerHTML = document.getElementById("dashboard-screen").innerHTML
    executeAjax("showDashboard", "GET", showDashboardData, "")
  }

  let links = ["customers-link", "employees-link", "transactions-link", "dashboard-link", "payables-link", "sign-out-link"]
  let templates = ["manage-customers-screen", "manage-employees-screen",
    "manage-deliveries-screen", "dashboard-screen", "manage-payables-screen", "log-in-screen"]
  for (let i = 0; i < 6; i++) {
    document.getElementById(links[i]).onclick = (event) => {
      event.preventDefault()
      container.innerHTML = document.getElementById(templates[i]).innerHTML
      switch (i) {
        case 0: executeAjax("/retrieveDocuments/customers", "GET", showCustomerInformations, "")
                break;
        case 1: executeAjax("/retrieveDocuments/employees", "GET", showEmployeeInformations, "")
                break;
        case 2: executeAjax("/retrieveDocuments/transactions", "GET", showTransactions, "")
                break;
        case 3: executeAjax("/showDashboard", "GET", showDashboardData, "")
                break;
        case 4: executeAjax("/retrieveDocuments/payables", "GET", showPayables, "")
                break;
        case 5: logOut()
                break;
      }
    }
  }
}

function logOut() {
  localStorage.removeItem('jwtToken')
  document.getElementById("header").style.display = "none"
}

function executeAjax(url, method, command, object) {
  const xhr = new XMLHttpRequest()
  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4 && xhr.status === 200) {
      if (command.length === 2) {
        command[0](JSON.parse(xhr.responseText), command[1])
      }
      else {
        command(JSON.parse(xhr.responseText))
      }
    }
  }
  xhr.open(method, url, true)
  xhr.setRequestHeader("Content-Type", "application/json")
  xhr.send(JSON.stringify(object))
}

function loginAdmin() {
  document.getElementById("status").style.display = "none"
  const userID = document.getElementById("userId").value
  const password = document.getElementById("password").value
  console.log("ADMIN INFO", userID, password)
  if (userID === "" || password === "") {
    document.getElementById("status").style.display = "block"
  }

  else {
    let url = `/loginAdmin`
    let admin = {user: userID, pass: password}
    executeAjax(url, "POST", showDashboardData, admin)
  }
}

function showDashboardData(results) {
  if (results.jwt != null) {
    console.log("Danddie")
    localStorage.setItem("jwtToken", results.jwt)
    executeAjax("/showDashboard", "GET", showDashboardData, "")
    document.getElementById("close-modal").click()
  }

  if (results.success != null && results.success === false) {
    document.getElementById("status").style.display = "block"
  }

  if (results.length === 2) {
    document.getElementById("content-container").innerHTML = document.getElementById("dashboard-screen").innerHTML
    document.getElementById("header").style.display = "block"
    for (const customer of results[0]) {
      let src = (customer.gender == "F") ? "assets/female-avatar.png" : "assets/male-avatar.png"
      document.getElementById("customer-overview").innerHTML +=
        `<div class="customer-info"><img src="${src}" 
                class="icon" /><span class="customer-details">${customer.name}</span>
            <br><span class="customer-details">${customer.number}</span>
            <br><span class="customer-details">${customer.address}</span></div>`
    }
    for (const employee of results[1]) {
      let src = (employee.gender == "F") ? "assets/female-avatar.png" : "assets/male-avatar.png"
      document.getElementById("employee-overview").innerHTML +=
        `<div class="employee-info"><img src="${src}" 
                class="icon" /><span class="employee-details">${employee.name}</span>
            <br><span class="employee-details">${employee.position}</span>
            <br><span class="employee-details">${employee.number}</span></div>`
    }
  }
}


