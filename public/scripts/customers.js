const customerIDs = ["customer-name", "customer-contactNumber", "customer-email", "customer-address",
  "customer-objectId", "customer-gender"]
let transactions = []

function retrieveCustomer(objectID) {
  let url = `/retrieveDocument/${encodeURIComponent(objectID)}&customers`
  executeAjax(url, "GET", prefillCustomerForm, "")
}

function prefillCustomerForm(customer) {
  executeAjax("/retrieveDocuments/customers", "GET", showCustomerInformations, "")
  let values = [customer.name, customer.number, customer.email, customer.address,
  customer._id, customer.gender]
  transactions = customer.transactions
  for (let i = 0; i < 6; i++) {
    setInputValue(customerIDs[i], values[i])
  }
}

function saveCustomerInfo() {
  let isIncomplete = "false"
  let values = []
  for (const id of customerIDs) {
    values.push(document.getElementById(id).value)
  }
  for (const value of values) {
    console.log(value)
    if (values.indexOf(value) != 4) {
      if (value === "") {
        alert("Form is Incomplete")
        isIncomplete = "true"
        break;
      }
    }
  }
  if (isIncomplete === "false") {
    if (document.getElementById("customer-objectId").value !== "") {   
      let customer = {name: values[0], gender: values[5], number: values[1], email: values[2], 
        address: values[3], objectId: values[4], transactions: transactions}
      let url = `/updateCustomerInfo`
      executeAjax(url, "POST", showCustomerInformations, customer)
    }

    else {
      let customer = {name: values[0], gender: values[5], number: values[1], email: values[2], address: values[3]}
      let url = `/saveCustomerInfo`
      executeAjax(url, "POST", showCustomerInformations, customer)
    }
    const ids = ["customer-name", "customer-contactNumber", "customer-email", "customer-address", "customer-objectId"]
    for (const id of ids) {
      document.getElementById(id).value = ""
    }
  }
}

function showCustomerInformations(customers) {
  document.getElementById("customers-list").innerHTML = ""
  let i = 0
  let j = 0
  for (const customer of customers) {
    let transactions = customer.transactions
    if ((i % 2) === 0) {
      j++
      document.getElementById("customers-list").innerHTML += `<div class="row" id="row-${j}" 
            style="margin:0px;"></div>`
    }
    getTransactionIndex(customer)
    document.getElementById(`row-${j}`).innerHTML +=
      `<div class="panel panel-default col-sm-6" style="padding:0px">
        <div class="panel-body customers-list-detail">
          <span><span class="glyphicon glyphicon-user"></span> ${customer.name}</span>
          <span class="glyphicon glyphicon-trash" style="float:right" 
          onclick='deleteCustomer("${customer._id}", "${customer.transactions}")'></span><br>
          <span><span class="glyphicon glyphicon-phone-alt"></span> ${customer.number}</span><br>
          <span><span class="glyphicon glyphicon-home"></span> ${customer.address}</span>
          <span class="glyphicon glyphicon-pencil" style="float:right" 
          onclick='retrieveCustomer("${customer._id}")'></span>
        </div>
        <div class="panel-footer">
          <center><span style="font-size:17px;font-weight:bold;
          font-family:Antipasto">Latest Transaction Data</span></center>
          <div id="${customer._id}" style="color:tomato;font-weight:bold">
            <center><span>Haven't Added Any Transactions Yet</span></center>
          </div>
        </div>
      </div>`
    i++
  }
}

function getTransactionIndex(customer) {
  if (customer.transactions != null) {
    let index = customer.transactions.length - 1
    const transactionId = customer.transactions[index]
    console.log(transactionId)
    let url = `/getLatestTransaction/${encodeURIComponent(transactionId)}`
    let array = [fillInTransactionDiv, customer._id]
    console.log(array)
    executeAjax(url, "GET", array, "")
  }
}

function fillInTransactionDiv(transaction, id) {
  document.getElementById(id).innerHTML = `<center><span>Unreturned Bottles: ${transaction.numOfUB}</span><br>
    <span>Credit: ${transaction.credit}</span><br><span>Transaction Date: ${transaction.date}</span></center>`
}

function deleteCustomer(objectID, transactions) {
  console.log("origID", transactions)
  console.log("length", transactions.length)
  let url = `/deleteDocument/${encodeURIComponent(objectID)}&customers`
  executeAjax(url, "GET", showCustomerInformations, "")
  deleteAssociatedTransactions(transactions)
}

function deleteAssociatedTransactions(transactionIds) {
  transactionIds = transactionIds.split(",")
  for (id of transactionIds) {
    let url = `/deleteTransaction/${encodeURIComponent(id)}`
    executeAjax(url, "GET", showCustomerInformations, "")
  }
}


