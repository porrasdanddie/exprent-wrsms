const payableIDs = ["for-what", "amount", "details", "payable-id", "month-options", "day-options"]

function encodePayableInputValue(id) {
  if (id === ("month-options" || "day-options")) {
    const input = document.getElementById(id)
      return encodeURIComponent(input.options[input.selectedIndex].value)
  }
  return encodeURIComponent(document.getElementById(id).value)
}

function retrievePayable(objectID) {
  document.getElementById("payable-id").value = objectID
  let url = `/retrieveDocument/${encodeURIComponent(objectID)}&payables`
  executeAjax(url, "GET", prefillPayableForm, "")
}

function deletePayable(objectID) {
  const id = encodeURIComponent(objectID)
  let url = `/deleteDocument/${id}&payables`
  executeAjax(url, "GET", showPayables, "")
}

function prefillPayableForm(payable) {
  generateMonthAndDayOptions()
  const values = [payable.forWhat, payable.amount, payable.details, payable._id, payable.month, payable.day]
  for (let i = 0; i < 6; i++) {
    setInputValue(payableIDs[i], values[i])
  }
  executeAjax('/retrieveDocuments/payables', "GET", showPayables, "")
}

function generateMonthAndDayOptions() {
  document.getElementById("month-options").innerHTML = ""
  document.getElementById("day-options").innerHTML = ""
  const months = ["January", "February", "March", "April", "May", "June", "July", 
  "August", "September", "October", "November", "December"]
  for (const month of months) {
      document.getElementById("month-options").innerHTML += `<option value="${month}">${month}</option>`
  }
  for (let i = 1; i < 32; i++) {
      document.getElementById("day-options").innerHTML += `<option value="${i}">${i}</option>`
  }   
}

function showPayables(payables) {
  document.getElementById("payables-list").innerHTML = ""
  for (const payable of payables) {
    console.log(payable._id)
    document.getElementById("payables-list").innerHTML += 
    `<div class="payables-data">
        <span>Payment Date: ${payable.month} ${payable.day}</span>
        <span class="glyphicon glyphicon-trash" style="float:right" 
        onclick='deletePayable("${payable._id}")'></span><br>
        <span>For: ${payable.forWhat}</span><br> 
        <span>Amount: ${payable.amount}</span><br>
        <span>More Details: ${payable.details}</span>
        <span class="glyphicon glyphicon-pencil" style="float:right" 
        onclick='retrievePayable("${payable._id}")'></span>
    </div>`
  }

  if (document.getElementById("payable-id").value === "") {
    generateMonthAndDayOptions()
  }

  document.getElementById("add-payable").onclick = () => {
    let values = []
    for (const id of payableIDs) {
      values.push(document.getElementById(id).value)
    }
    if (document.getElementById("payable-id").value !== "") {
        let payable = {month: values[4], day: values[5], forWhat: values[0], amount: values[1], 
          details: values[2], objectId: values[3]}
        let url = `/updatePayable`
        executeAjax(url, "POST", showPayables, payable)
        generateMonthAndDayOptions()
    }

    else {
        let payable = {month: values[4], day: values[5], forWhat: values[0], amount: values[1], details: values[2]}
        let url = `/addPayable`
        executeAjax(url, "POST", showPayables, payable)
        generateMonthAndDayOptions()
    }
    document.getElementById("for-what").value = ""
    document.getElementById("amount").value = ""
    document.getElementById("details").value = ""
  }
}