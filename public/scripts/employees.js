const ids = ["employee-name", "employee-number", "employee-position",
  "employee-salary", "employee-objectId", "employee-gender"]
let CAs = []
let absences = []

function setInputValue(id, value) {
  document.getElementById(id).value = value
}

function encodeInputValue(id) {
  console.log(id)
  if (id === ("employee-gender" || "customer-gender")) {
    const input = document.getElementById(id)
    return encodeURIComponent(input.options[input.selectedIndex].value)
  }
  return encodeURIComponent(document.getElementById(id).value)
}

function prefillEmployeeForm(employee) {
  executeAjax("/retrieveDocuments/employees", "GET", showEmployeeInformations)
  const values = [employee.name, employee.number, employee.position,
  employee.salary, employee._id, employee.gender]
  CAs = employee.CAs
  absences = employee.absences
  for (let i = 0; i < 6; i++) {
    setInputValue(ids[i], values[i])
  }
}

function saveEmployeeInfo() {
  let isIncomplete = "false"
  let values = []
  for (const id of ids) {
    values.push(document.getElementById(id).value)
  }
  for (const value of values) {
    if (values.indexOf(value) != 4) {
      if (value === "") {
        alert("Form is Incomplete")
        isIncomplete = "true"
        break;
      }
    }
  }
  if (isIncomplete === "false") {
    if (document.getElementById("employee-objectId").value !== "") {
      let employee = {name: values[0], gender: values[5], number: values[1], position: values[2], 
        salary: values[3], objectId: values[4], CAs: CAs, absences: absences}
      let url = `/updateEmployeeInfo`
      executeAjax(url, "POST", showEmployeeInformations, employee)
    }

    else {
      let employee = {name: values[0], gender: values[5], number: values[1], position: values[2], salary: values[3]}
      let url = `/saveEmployeeInfo`
      executeAjax(url, "POST", showEmployeeInformations, employee)
    }
    const ids = ["employee-name", "employee-number", "employee-position", "employee-salary", "employee-objectId"]
    for (const id of ids) {
      document.getElementById(id).value = ""
    }
  }
}

function showEmployeeInformations(employees) {
  const inputIds = ["employee-search-a-div", "employee-search-ca-div", "absence-date",
    "employee-search-a", "employee-search-ca", "amount", "date-claimed"]
  const divIds = ["employees-list", "ca-search", "absences-search"]
  let i = 0
  let j = 0
  for (const id of inputIds) {
    document.getElementById(id).value = ""
  }
  for (const id of divIds) {
    document.getElementById(id).innerHTML = ""
  }
  viewResultsInAbsenceForm(employees)
  viewResultsInCAForm(employees)
  for (const employee of employees) {
    let numOfAbsences = 0
    if (employee.absences != null) {
      numOfAbsences = employee.absences.length 
    }

    if ((i % 3) === 0) {
      j++
      document.getElementById("employees-list").innerHTML += `<div class="row" id="row-${j}" 
          style="margin:0px;"></div>`
    }
    document.getElementById(`row-${j}`).innerHTML +=
      `<div class="panel panel-default col-sm-4" style="padding:0px;">
        <div class="panel-body employees-list-detail">
          <span><span class="glyphicon glyphicon-user"></span> ${employee.name}</span>
          <span class="glyphicon glyphicon-trash" style="float:right" 
          onclick='deleteEmployee("${employee._id}")'></span><br>
          <span><span class="glyphicon glyphicon-phone-alt"></span> ${employee.number}</span><br>
          <span><span class="glyphicon glyphicon-briefcase"></span> ${employee.position}</span><br>
          <span><span class="glyphicon glyphicon-usd"></span> ${employee.salary}</span>
          <span class="glyphicon glyphicon-pencil" style="float:right" 
          onclick='retrieveEmployee("${employee._id}")'></span>
        </div>
        <div class="panel-footer" id="${employee._id}">
          <center style="margin-bottom:5px;"><span class="label label-danger" style="padding:6px;">
          Number of Absences  <span class="badge">${numOfAbsences}</span</span></center>
        </div>
      </div>`
    i++
    if (employee.CAs != null) {
      solveTotalCashAdvances(employee.CAs, employee._id)
    }

    else {
      document.getElementById(employee._id).innerHTML += `<center><span class="label label-warning" 
      style="padding:6px;">Total Cash Advance  <span class="badge">0</span</span></center>`
    }
    document.getElementById("ca-search").innerHTML +=
      `<option value="${employee.name}">${employee.name}</option>`
    document.getElementById("absences-search").innerHTML +=
      `<option value="${employee.name}">${employee.name}</option>`
  }

  document.getElementById("ca-search-button").onclick = () => {
    executeAbsenceSearch("ca-search", showCashAdvances)
  }

  document.getElementById("absences-search-button").onclick = () => {
    executeAbsenceSearch("absences-search", showAbsences)
  }

  document.getElementById("add-ca").onclick = () => {
    document.getElementById("ca-list").innerHTML = ""
    const date = encodeURIComponent(document.getElementById("date-claimed").value)
    const name = encodeURIComponent(document.getElementById("employee-search-ca").value)
    const amount = encodeURIComponent(document.getElementById("amount").value)
    let url = `/addCashAdvance/${date}&${name}&${amount}`
    executeAjax(url, "POST", showEmployeeInformations)
  }

  document.getElementById("add-absence").onclick = () => {
    document.getElementById("absences-list").innerHTML = ""
    const date = encodeURIComponent(document.getElementById("absence-date").value)
    const name = encodeURIComponent(document.getElementById("employee-search-a").value)
    let url = `/addAbsence/${date}&${name}`
    executeAjax(url, "POST", showEmployeeInformations)
  }
}

function solveTotalCashAdvances(cashAdvances, id) {
  let totalCAs = 0
  for (const ca of cashAdvances) {
    totalCAs += parseInt(ca.amount)
  }
  console.log("HORRRAYYY", id)
  document.getElementById(id).innerHTML += `<center><span class="label label-warning" 
  style="padding:6px;">Total Cash Advance  <span class="badge">${totalCAs}</span</span></center>`
}

function executeAbsenceSearch(id, command) {
  const nameOptions = document.getElementById(id)
  const name = nameOptions.options[nameOptions.selectedIndex].value
  let url = `/retrieveDocumentUsingName/${name}&employees`
  executeAjax(url, "GET", command)
}

function executeEmployeeSearch(inputID) {
  const input = document.getElementById(inputID)
  const command = inputID === "employee-search-a" ? viewResultsInAbsenceForm : viewResultsInCAForm
  const url = `/employeeSearch/${encodeURIComponent(input.value)}`
  if (input.value === "") {
    executeAjax("/retrieveDocuments/employees", "GET", command)
  }
  executeAjax(url, "POST", command)
}

function retrieveEmployee(objectID) {
  console.log("employee update...")
  let url = `/retrieveDocument/${encodeURIComponent(objectID)}&employees`
  console.log(url)
  executeAjax(url, "GET", prefillEmployeeForm)
}

function deleteEmployee(objectID) {
  let url = `/deleteDocument/${encodeURIComponent(objectID)}&employees`
  console.log(url)
  executeAjax(url, "GET", showEmployeeInformations)
}

function setCANameField(name) {
  console.log(name)
  document.getElementById("employee-search-ca").value = name
}

function setANameField(name) {
  console.log(name)
  document.getElementById("employee-search-a").value = name
}

function viewResultsInAbsenceForm(employees) {
  document.getElementById("employee-search-a-div").innerHTML = ""
  for (const employee of employees) {
    document.getElementById("employee-search-a-div").innerHTML +=
      `<div class="employee-search-data" 
          onclick='setANameField("${employee.name}")'><span>${employee.name}</span></div>`
  }
}

function viewResultsInCAForm(employees) {
  document.getElementById("employee-search-ca-div").innerHTML = ""
  for (const employee of employees) {
    document.getElementById("employee-search-ca-div").innerHTML +=
      `<div class="employee-search-data" 
          onclick='setCANameField("${employee.name}")'>${employee.name}</div>`
  }
}

function showCashAdvances(employee) {
  const cashAdvances = employee.CAs
  console.log(cashAdvances)
  let list = document.getElementById("ca-list")
  list.innerHTML = `<center><span id="no-results-ca" style="display:none">No Results</span><center>`
  if (cashAdvances == null) {
    document.getElementById("no-results-ca").style.display = "block";
  }

  else {
    for (const ca of cashAdvances) {
      document.getElementById("no-results-ca").style.display = "hidden";
      list.innerHTML += `<br><center><div class="infos">
          <span class="glyphicon glyphicon-calendar"></span><span>${ca.date}</span><br>
          <span class="glyphicon glyphicon-usd"></span><span>${ca.amount}</span></div><center>`
    }
  }
}

function showAbsences(employee) {
  const absences = employee.absences
  console.log(absences)
  let list = document.getElementById("absences-list")
  list.innerHTML = `<center><span id="no-results-a" style="display:none">No Results</span><center>`
  if (absences == null) {
    document.getElementById("no-results-a").style.display = "block";
  }

  else {
    for (const absence of absences) {
      document.getElementById("no-results-a").style.display = "hidden";
      list.innerHTML += `<center><div class="infos">
          <span class="glyphicon glyphicon-calendar"></span><span>${absence.date}</span></div></center>`
    }
  }
}
