const transactionIDs = ["userInput", "bottles-delivered", "bottles-returned", 
"amountDue", "amountPaid", "unreturned-bottles", "debt", "transaction-date"]

function showDiv(id) {
  document.getElementById(`${id}-details`).style.display = "block"
  document.getElementById(`show-${id}-details`).style.transform = "rotateX(180deg)"
  document.getElementById(`show-${id}-details`).onclick = () => {
      hideDiv(id)
  }
}

function hideDiv(id) {
  document.getElementById(`${id}-details`).style.display = "none"
  document.getElementById(`show-${id}-details`).style.transform = "rotateY(180deg)"
  document.getElementById(`show-${id}-details`).onclick = () => {
      showDiv(id)
  }
}

function showTransactions(transactions) {
  document.getElementById("transactions-list").innerHTML = ""
  for (const transaction of transactions) {
      const id = transaction._id
      document.getElementById("transactions-list").innerHTML += 
      `<div class="transactions-list-data">
          <span>Transaction ID: ${id}</span><br>
          <span>Name of Customer: ${transaction.nameOfCustomer}</span>
          <span class="glyphicon glyphicon-chevron-down" id="show-${id}-details" 
          onclick='showDiv("${id}")' style="float:right"></span>
          <div id="${id}-details" style="display:none">
              <center><span>Delivered Bottles: ${transaction.numOfDB}</span><br>
              <span>Returned Bottles: ${transaction.numOfRB}</span><br>
              <span>Unreturned Bottles: ${transaction.numOfUB}</span><br>
              <span>Amount Due: ${transaction.amountDue}</span><br>
              <span>Amount Paid: ${transaction.amountPaid}</span><br>
              <span>Credit: ${transaction.credit}</span><br>
              <span>Transaction Date: ${transaction.date}</span><br></center>
          </div>
      </div>`
  }
  for (const id of transactionIDs) {
    document.getElementById(id).value = ""
  }
  document.getElementById("userInput").onkeyup = () => {
      document.getElementById("customers-list-trans").style.display = 'block'
      const name = document.getElementById("userInput").value
      if (name === "") {
          executeAjax("/retrieveDocuments/customers", "GET", viewResultsInTransactionForm, "")
      }
      else {
          let url = `/customerSearch/${name}`
          executeAjax(url, "GET", viewResultsInTransactionForm, "")
      }
  }

  const bottleDataInputs = ["bottles-delivered", "bottles-returned"]
  for (const id of bottleDataInputs) {
      document.getElementById(id).onkeyup = () => {
          let numOfDB = document.getElementById("bottles-delivered").value
          let numOfRB = document.getElementById("bottles-returned").value
          let latestNumOfUB = document.getElementById("latestNumOfUB").value
          const values = [numOfDB, latestNumOfUB]
          document.getElementById("unreturned-bottles").value = solveInputs(numOfDB, numOfRB, latestNumOfUB)         
      }
  }

  const paymentDataInputs = ["amountDue", "amountPaid"]
  for (const id of paymentDataInputs) {
    document.getElementById(id).onkeyup = () => {
      let amountDue = document.getElementById("amountDue").value
      let amountPaid = document.getElementById("amountPaid").value
      let latestCredit = document.getElementById("latestCredit").value
      document.getElementById("debt").value = solveInputs(amountDue, amountPaid, latestCredit)
    }
  }

  document.getElementById("submit-transaction").onclick = () => {
      let values = []
      for (const id of transactionIDs) {
        values.push(document.getElementById(id).value)
      }
      const transaction = {
        numOfDB: values[1], numOfRB: values[2], numOfUB: values[5], amountDue: values[3], amountPaid: values[4], 
        credit: values[6], date: values[7], nameOfCustomer: values[0]
      }
      let url = `/addTransaction`
      executeAjax(url, "POST", showTransactions, transaction)
  }   
}

function solveInputs(due, given, latest) {
  let inputs = [due, given, latest]
  console.log(inputs)
  for (let i = 0; i < 3; i++) {
    if(inputs[i] === "") {
      inputs[i] = 0
    }

    else {
      inputs[i] = parseInt(inputs[i])
    }
  }
  return (inputs[0] + inputs[2]) - inputs[1]
}

function getLatestTransaction(result) {
  console.log("Result", result.transactions)
  if (result.transactions != null) {
    let index = result.transactions.length - 1
    const transactionId = result.transactions[index]
    console.log(transactionId)
    let url = `/getLatestTransaction/${encodeURIComponent(transactionId)}`
    executeAjax(url, "GET", setLatestNumberOfUnreturnedBottles, "")
  }
}

function setTransactionNameField(name) {
  document.getElementById("userInput").value = name
  document.getElementById("customers-list-trans").style.display = 'none'
  let url = `/retrieveDocumentUsingName/${encodeURIComponent(name)}&customers`
  executeAjax(url, "GET", getLatestTransaction, "")
}

function setLatestNumberOfUnreturnedBottles(transaction) {
  document.getElementById("latestNumOfUB").value = transaction.numOfUB
  setLatestCredit(transaction)
}

function setLatestCredit(transaction) {
  document.getElementById("latestCredit").value = transaction.credit   
}

function viewResultsInTransactionForm(customers) {
  document.getElementById("customers-list-trans").innerHTML = ""
  document.getElementById("customers-list-trans").innerHTML += 
  '<center><br><div id="customers-div"></div><center>'
  for (const customer of customers) {
      document.getElementById("customers-div").innerHTML += 
      `<div class="customer-search-data" onclick='setTransactionNameField("${customer.name}")'>
      <span>${customer.name}</span></div>`
  }
}   