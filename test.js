const inputSolver = require("./inputSolver.js")
const assert = require("chai")

describe('Input Solver', () => {
    describe("solve user inputs in transaction form", () => {
        it("solves total credit or unreturned bottles", () => {
            const actual = inputSolver(500, 150, 50)
            assert.expect(actual).to.equal(400)
        })
    })
})
