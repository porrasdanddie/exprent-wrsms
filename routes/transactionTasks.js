module.exports = function handleTransactionTasks(server, ObjectId, db) {
  server
    .post("/addTransaction", (request, response) => {
      const transaction = request.body
      db.collection("transactions").insertOne(transaction, (insertError) => {
        db.collection("customers").updateOne({ name: transaction.nameOfCustomer }, { $addToSet: 
          { transactions:  transaction._id  } }, (updateError) => {
            db.collection("transactions").find().toArray((error, transactions) => {
              if (error) throw error
              response.json(transactions)
            })
          })

      })
    })

    .get("/getLatestTransaction/:id", (request, response) => {
      const id = ObjectId(request.params.id)
      db.collection("transactions").findOne({_id: id}, (error, transaction) => {
        response.json(transaction)
      })
    })

    .get("/deleteTransaction/:objectId", (request, response) => {
      const objectId = request.params.objectId
      const id = new ObjectId(objectId)
      db.collection("transactions").deleteOne({ _id: id }, (error, result) => {
        db.collection("customers").find().toArray((error, customers) => {
          response.json(customers)
        })
      })
    })
}