module.exports = function handleCustomerTasks(server, ObjectId, db) {
  server
    .post("/saveCustomerInfo", (request, response) => {
      const customer = request.body
      console.log(customer)
      db.collection("customers").insertOne(customer, (insertError) => {
        if (insertError) throw insertError
        db.collection("customers").find().toArray((error, customers) => {
          if (error) throw error
          response.json(customers)
          response.end()
        })
      })
    })

    .post("/updateCustomerInfo", (request, response) => {
      const {name, gender, number, email, address, objectId, transactions} = request.body
      const customer = {name: name, gender: gender, number: number, email: email, address: address, transactions: transactions}
      const id = new ObjectId(request.body.objectId)
      db.collection("customers").updateOne({ _id: id }, customer, (updateError) => {
        if (updateError) throw updateError
        db.collection("customers").find().toArray((error, customers) => {
          if (error) throw error
          response.json(customers)
          response.end()
        })
      })
    })

    .get("/customerSearch/:input", (request, response) => {
      const input = request.params.input
      db.collection("customers").find({ name: { $regex: `^${input}`, $options: "i" } }).toArray((error, customers) => {
        if (error) throw error
        console.log(customers)
        response.json(customers)
        response.end()
      })
    })
}