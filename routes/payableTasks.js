module.exports = function handlePayableTasks(server, ObjectId, db) {
  server
    .post("/addPayable", (request, response) => {
      const payable = request.body
      db.collection("payables").insertOne(payable, (insertError) => {
        db.collection("payables").find().toArray((error, payables) => {
          response.json(payables)
        })
      })
    })

    .post("/updatePayable", (request, response) => {
      const {month, day, forWhat, amount, details, objectId} = request.body
      const id = new ObjectId(request.body.id)
      const payable = { month: month, day: day, forWhat: forWhat, amount: amount, details: details }
      db.collection("payables").updateOne({ _id: id }, payable, (updateError) => {
        if (updateError) throw updateError
        db.collection("payables").find().toArray((error, payables) => {
          if (error) throw error
          response.json(payables)
        })
      })
    })
}