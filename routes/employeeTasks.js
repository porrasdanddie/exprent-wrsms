module.exports = function handleEmployeeTasks(server, ObjectId, db) {
  server
    .post("/employeeSearch/:input", (request, response) => {
      const input = request.params.input
      db.collection("employees").find({ name: { $regex: `^${input}`, $options: "i" } }).toArray((error, employees) => {
        if (error) throw error
        console.log(employees)
        response.json(employees)
      })
    })

    .post("/updateEmployeeInfo", (request, response) => {
      const {name, gender, number, position, salary, objectId, CAs, absences} = request.body
      const employee = {name: name, gender: gender, number: number, position: position, salary: salary, 
        CAs: CAs, absences: absences}
      const id = new ObjectId(request.body.objectId)
      db.collection("employees").updateOne({ _id: id }, employee, (updateError) => {
        if (updateError) throw updateError
        db.collection("employees").find().toArray((error, employees) => {
          if (error) throw error
          response.json(employees)
        })
      })
    })
    
    .post("/saveEmployeeInfo", (request, response) => {
      const employee = request.body
      db.collection("employees").insertOne(employee, (insertError) => {
        if (insertError) throw insertError
        db.collection("employees").find().toArray((error, employees) => {
          if (error) throw error
          response.json(employees)
        })
      })
    })

    .post("/addAbsence/:date&:name", (request, response) => {
      const name = request.params.name
      const date = request.params.date
      db.collection("employees").updateOne({ name: name }, { $addToSet: { absences: { date: date } } }, (updateError) => {
        db.collection("employees").find().toArray((error, employees) => {
          response.json(employees)
        })
      })
    })

    .post("/addCashAdvance/:date&:name&:amount", (request, response) => {
      const name = request.params.name
      const date = request.params.date
      const amount = request.params.amount
      db.collection("employees").updateOne({ name: name }, { $addToSet: { CAs: { date: date, amount: amount } }
      }, (updateError) => {
        db.collection("employees").find().toArray((error, employees) => {
          response.json(employees)
        })
      })
    })
}