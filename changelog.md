<a name="1.0.0"></a>
# 1.0.0 (2017-11-07)


### Bug Fixes

* add start script in package.json ([ee25281](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/ee25281))
* change location of index.html ([e1c393c](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/e1c393c))
* fix absences search bug ([2f20709](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/2f20709))
* fix add customer bug ([1691e8a](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/1691e8a))
* fix add customer bug ([368b652](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/368b652))
* fix add payable bug ([d9d8285](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/d9d8285))
* fix add transaction method ([dafa429](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/dafa429))
* fix bcrypt module issue ([054d7da](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/054d7da))
* fix bug in customer display ([0877073](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/0877073))
* fix bug in displaying transactions ([ac735df](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/ac735df))
* fix bug in employee search ([d44d7ae](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/d44d7ae))
* fix css for transactions display ([0e64bf1](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/0e64bf1))
* fix customer display ([029f189](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/029f189))
* fix customer display ([5aaee38](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/5aaee38))
* fix customer display ([1c3f1b9](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/1c3f1b9))
* fix delete transaction bug ([faab06c](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/faab06c))
* fix delete transaction method ([5903c93](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/5903c93))
* fix deleteTransaction func ([f64f85e](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/f64f85e))
* fix employee display ([4caf0b1](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/4caf0b1))
* fix employee search bug ([31f5dfa](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/31f5dfa))
* fix getLatestTransaction method ([fd32c73](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/fd32c73))
* fix getTransactionIndex method ([2c64416](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/2c64416))
* fix login ([4dea836](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/4dea836))
* fix login ([03b0bba](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/03b0bba))
* fix login ([40a77d6](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/40a77d6))
* fix login ([14d515a](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/14d515a))
* fix logout feat ([c8bbd2a](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/c8bbd2a))
* fix minor bugs ([97ae276](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/97ae276))
* fix minor dashboard bug ([3d159d3](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/3d159d3))
* fix modal bug ([7947d5f](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/7947d5f))
* fix null db ([ef97ede](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/ef97ede))
* fix payable form ([103e2fd](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/103e2fd))
* fix payable form ([1f4e1c9](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/1f4e1c9))
* fix port issue ([da4d18c](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/da4d18c))
* fix port issue ([6d5155a](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/6d5155a))
* fix scrollbar in dashboard ([77c5d75](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/77c5d75))
* fix scrollbar in dashboard overviews ([9345678](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/9345678))
* fix several bugs ([91a8bbf](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/91a8bbf))
* fix several form bugs ([e0b193c](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/e0b193c))
* fix solveTotalCAs function ([377bbc5](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/377bbc5))
* fix transaction form bug ([b3a0214](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/b3a0214))
* fix transaction form bug ([a5c9125](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/a5c9125))
* fix undefined variable in customers.js ([2897546](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/2897546))
* fix update payable bug ([4766996](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/4766996))
* fix update payable bug ([56de646](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/56de646))
* minor css fix ([ddcbaaf](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/ddcbaaf))
* minor css fix ([0605f07](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/0605f07))
* minor fix in employee display margin ([ba3cf62](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/ba3cf62))
* minor prefill bug ([408f9ee](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/408f9ee))
* modify window.onload ([666226c](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/666226c))
* several css fixes ([ee99fc1](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/ee99fc1))


### Features

* add admin log-in feature ([aa61852](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/aa61852))
* add dashboard and queries to show employees and customers ([7ef687e](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/7ef687e))
* add delete feature for employees ([1252708](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/1252708))
* add features for customers and employees ([f857e0f](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/f857e0f))
* add insert, update and delete features for payables ([2f12461](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/2f12461))
* add invalid admin info warning ([0f53eb1](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/0f53eb1))
* add navbar and sign-in modal ([6bf07b4](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/6bf07b4))
* add sign out feature ([585e80c](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/585e80c))
* add transaction feature and logic in transaction form ([e20659e](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/e20659e))
* add update employee feature ([51371ed](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/51371ed))
* host app using heroku and mlab ([a10298a](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/a10298a))
* insert customer and employee data to database ([a42f248](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/a42f248))


### Performance Improvements

* fix spa and rename html file ([1e62bb3](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/1e62bb3))



<a name="1.0.0"></a>
# 1.0.0 (2017-11-02)


### Bug Fixes

* fix bug in employee search ([d44d7ae](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/d44d7ae))


### Features

* add dashboard and queries to show employees and customers ([7ef687e](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/7ef687e))
* add delete feature for employees ([1252708](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/1252708))
* add features for customers and employees ([f857e0f](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/f857e0f))
* add insert, update and delete features for payables ([2f12461](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/2f12461))
* add navbar and sign-in modal ([6bf07b4](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/6bf07b4))
* add transaction feature and logic in transaction form ([e20659e](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/e20659e))
* add update employee feature ([51371ed](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/51371ed))
* insert customer and employee data to database ([a42f248](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/a42f248))


### Performance Improvements

* fix spa and rename html file ([1e62bb3](https://gitlab.com/porrasdanddie/exprent-wrsms/commit/1e62bb3))
